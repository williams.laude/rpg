import Character from "./characters/character";

export default class Battlesim {
    arena(character1: Character, enemy: Array<Character>){
        if(!character1 || !enemy){
            console.log('Il manque un joueur !')
            return;
        }
        console.log(" ---------- DEBUT COMBAT ----------")

        let tour = 0;
        let enemynumber = 0;
        //systeme de combat tant que le hero est vivant/qu'il reste des ennemis vivants
        while(enemynumber !== enemy.length)
        {
            let tour = 0
            let character2 = enemy[enemynumber]
        /*syteme de levelup 
        Pour chaque participants
        */
        character1.setLvl()
        character2.setLvl()
        let storeHealth = character2.getHealth()
        /*systeme de single combat */
        if (character1.getHealth() > 0){
        console.log(`---------- Combat de notre Héro contre ${character2.getName()} ------------`)
        console.log('\n' + '\n')
        }
        while (character1.getHealth() > 0 && character2.getHealth() > 0) {
            tour++
            let damage1 = character1.attack(character2.state)

            console.log("---------- Tour n°"+(tour)+" ----------")
            let storeHealth2 = character2.getHealth()
                console.log(character1.getName()+" inflige "+Math.round((damage1)*100)/100+" points de dégats à "+character2.getName())
                character2.setHealth(damage1)
                if ((storeHealth2-damage1) !== character2.getHealth()){
                console.log(character2.getName()+" résiste à "+Math.round((damage1-(storeHealth2-character2.getHealth()))*100)/100+" points de dommages")
                }
                console.log(character2.getName()+" à désormais = "+Math.round((character2.getHealth()*100)/100)+" HP")

            console.log("           ")

            if (character2.getHealth() > 0){
            let damage2 = character2.attack(character1.state)
            tour++
            let storeHealth1 = character1.getHealth()
            console.log("---------- Tour n°"+tour+" ----------")
                console.log(character2.getName()+" inflige "+Math.round(damage2*100)/100+" points de dégats à "+character1.getName())
                character1.setHealth(damage2)
                if ((storeHealth1-damage2) !== character1.getHealth()){
                console.log(character1.getName()+" résiste à "+Math.round((damage2-(storeHealth1-character1.getHealth()))*100)/100+" points de dommages")
                }
                console.log(character1.getName()+" à désormais = "+Math.round(character1.getHealth()*100)/100+" HP")
            }
            console.log("          ")
            if (character1.getHealth()<=0){
                character1.die()
                console.log("\n"+"\n"+"\n")
            }else if (character2.getHealth()<=0){
                character2.die()
                console.log("\n"+"\n"+"\n")
                character1.setXp()
                character1.health += storeHealth * 0.1
                console.log(`${character1.getName()} recupere ${storeHealth * 0.1} hp`)
                console.log('\n' + '\n')
            }
        }
            enemynumber++
        }
    }

}