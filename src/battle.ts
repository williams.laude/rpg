import Character from "./characters/character";

export default class Battle {
    fight(character1: Character, character2: Character){
        // on check que les 2 combattants sonts presents
        if (!character1 || !character2){
            console.log('il manque un ou plusieurs combattants');
            // on quitte la methode fight avec return
            return;
        }
        let tour = 0

        /*sytem levelup 
        Pour chaque participants
        */
        character1.setLvl()
        character2.setLvl()

        /*system single combat */
        let storeinitialHealth1 = character1.getHealth()
        let storeinitialHealth2 = character2.getHealth()
        while (character1.getHealth() > 0 && character2.getHealth() > 0) {
            tour++
            let damage1 = character1.attack(character2.state)

            console.log("---------- Tour n°"+(tour)+" ----------")
            let storeHealth2 = character2.getHealth()
                console.log(character1.getName()+" inflige "+Math.round((damage1)*100)/100+" points de dégats à "+character2.getName())
                character2.setHealth(damage1)
                if ((storeHealth2-damage1) !== character2.getHealth()){
                console.log(character2.getName()+" résiste à "+Math.round((damage1-(storeHealth2-character2.getHealth()))*100)/100+" points de dommages")
                }
                console.log(character2.getName()+" à désormais = "+Math.round((character2.getHealth()*100)/100)+" HP")

            console.log("           ")

            if (character2.getHealth() > 0){
            let damage2 = character2.attack(character1.state)
            tour++
            let storeHealth1 = character1.getHealth()
            console.log("---------- Tour n°"+tour+" ----------")
                console.log(character2.getName()+" inflige "+Math.round(damage2*100)/100+" points de dégats à "+character1.getName())
                character1.setHealth(damage2)
                if ((storeHealth1-damage2) !== character1.getHealth()){
                console.log(character1.getName()+" résiste à "+Math.round((damage2-(storeHealth1-character1.getHealth()))*100)/100+" points de dommages")
                }
                console.log(character1.getName()+" à désormais = "+Math.round(character1.getHealth()*100)/100+" HP")
            }
            console.log("          ")
            if (character1.getHealth()<=0){
                character1.die()
                console.log("\n"+"\n"+"\n")
                character2.setXp()
                character2.health += storeinitialHealth1 * 0.1
                console.log(`${character2.getName()} recupere ${storeinitialHealth1 * 0.1} hp`)
                console.log('\n' + '\n')
            }else if (character2.getHealth()<=0){
                character2.die()
                console.log("\n"+"\n"+"\n")
                character1.setXp()
                character1.health += storeinitialHealth2 * 0.1
                console.log(`${character1.getName()} recupere ${storeinitialHealth2 * 0.1} hp`)
                console.log('\n' + '\n')
            }
        }

    }
}