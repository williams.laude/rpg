import Character from "./character";

export default class Golem extends Character{
    constructor(name: string ,health: number, hitStrength: number,xp: number){
        super(name, health, hitStrength, xp)
    }

    setHealth(damage: number) {
        let rand = Math.floor(Math.random() * 2)
        if (rand == 1){
            return this.health -= damage;
        }else if (rand == 0) {
            return this.health;
        }
    }
}
