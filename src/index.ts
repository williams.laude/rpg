import Battle from "./battle";
import Hero from './characters/hero'
import Character from "./characters/character";
import Berseker from "./characters/berseker"
import Werewolf from "./characters/werewolf"
import Golem from "./characters/golem"
import Assasin from "./characters/assassin"
import Griffin from "./characters/griffin"
import Dragon from "./characters/dragon"
import BattleSimulation from "./arenabattle";
import Battlesim from "./battlesim"

let char1 = new Character ('bobmorane', 200 , 10 , 30)
let char2 = new Assasin ('chacal', 200 , 10 , 0)

let c1 = new Hero ('Bob Morane', 28000 , 1850 , 0,'Dwarf')
let c2 = new Griffin ('Griffin', 2300 , 1800 , 0)
let c3 = new Assasin ('Assassin', 900, 2700, 0)
let c4 = new Berseker ('Berserker', 5500, 800, 0)
let c5 = new Dragon ('Dragon', 3200, 3700, 0)
let c6 = new Golem ('Golem', 8000, 1000, 0)
let c7 = new Werewolf ('Werewolf', 2000, 1300, 0)
let c8 = new Hero ('Le Nain', 20285, 500, 60,'Dwarf')
let enemy = [
    c2,c3,c4,c5,c6,c7,c8
]

// let battle = new Battle ()
// battle.fight(c5,c6)

let battlesim = new Battlesim()
battlesim.arena(c1,enemy)

// let battle = new Battle ()
// battle.fight(char1,char2)

// let arenebattle = new BattleSimulation ()
// arenebattle.arene(enemy)