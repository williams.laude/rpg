import Character from "./character";

export default class Griffin extends Character{

    attackCycleOrder: number = 0
    state: string = "ground"

    constructor(name: string ,health: number, hitStrength: number,xp: number){
        super(name, health, hitStrength, xp)
    }

    
    fly(): number {
        return this.hitStrength * this.level * 0
    }


    attackFromSky(): number {
        return this.hitStrength * this.level * 1.1
    }


    attack(): number {
        if (this.attackCycleOrder == 0){
            this.attackCycleOrder++
            this.state = "ground"
            return this.hitStrength * this.level
        }
        else if (this.attackCycleOrder == 1){
            this.attackCycleOrder++
            this.state = "flying"
            return this.fly()
        }
        else if (this.attackCycleOrder == 2){
            this.attackCycleOrder = 0
            this.state = "ground"
            return this.attackFromSky()
        }
    }
        setHealth(damage: number) {
            if (this.state == "ground"){
                console.log("Le Griffon est au sol")
                return this.health -= damage
            }
            else if (this.state == "flying"){
                console.log("Le Griffon est en l'air")
                return this.health -= damage * 0.9
            }

        }
}
