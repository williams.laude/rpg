export default class Character {
    level: number = 1;
    name: string;
    health: number;
    hitStrength: number;
    xp: number;
    state: string = "ground"
    constructor(name: string, health: number, hitStrength: number, xp: number) {
        this.name = name;
        this.health = health;
        this.hitStrength = hitStrength;
        this.xp = xp;
    }

    getName(): string {
        return this.name;
    }

    setName(name: string) {
        this.name = name;
    }

    getHealth(): number {
        return this.health;
    }


    setHealth(damage: number) {
        this.health -= damage;
    }

    getLvl(): number {
        return this.level;
    }

    setLvl() : void {
        while (this.xp >= 10) {
            this.xp -= 10
            this.level += 1
        }
    }

    getXp(): number {
        return this.xp;
    }

    setXp(): void {
        this.xp += 2;
    }

    getHitStrength(): number {
        return this.hitStrength;
    }

    setHitStrength(hitStrength: number) {
        this.hitStrength;
    }

    attack(state: string): number {
        return this.hitStrength * this.level
    }

    die(): void {
        return console.log(`${this.name} est mort, il a ${this.getHealth()} points de vie`)
    }
}