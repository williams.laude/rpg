import Character from "./character";

export default class Assassin extends Character{
    constructor(name: string ,health: number, hitStrength: number,xp: number){
        super(name, health, hitStrength, xp)
    }
    attack(): number {
        this.hitStrength = this.hitStrength*1.1
        return this.hitStrength * this.level
    }
}
