import Character from "./character";

export default class Hero extends Character{
    race: string;

    constructor(name: string ,health: number, hitStrength: number,xp: number, race: string){
        super(name, health, hitStrength, xp)
        this.race = race;
    }

    setHealth(damage: number) {
    if (this.race == "Dwarf"){
        return this.dwarfHealth(damage)
    }
    else {
        return this.health -= damage;
    }
}

    dwarfHealth(damage: number){
        let rand = Math.floor(Math.random() * 10)
        if (rand >= 2){
            return this.health -= damage;
        }else if (rand < 2) {
            return this.health -= damage * 0.5
        }
    }

    attack(state: string): number {
        if (this.race == "Elf"){
            return this.elfAttack(state)
        }
        else if (this.race == "Humans"){
            return this.humansAttack(state)
        }
        else {
            return this.hitStrength * this.level
        }
    }

    elfAttack(state : string) : number {
        if (state == "flying"){
            return (this.hitStrength * 1.1) * this.level
        }else{
            return (this.hitStrength * 0.9) * this.level
        }
    }

    humansAttack(state : string) : number {
        if (state == "ground"){
            return (this.hitStrength * 1.1) * this.level
        }else{
            return (this.hitStrength * 0.9) * this.level
        }
    }

}