import Character from "./character";

export default class Werewolf extends Character{
    constructor(name: string ,health: number, hitStrength: number,xp: number){
        super(name, health, hitStrength, xp)
    }

    setHealth(damage: number) {
        this.health -= (damage*0.5);
    }
}
