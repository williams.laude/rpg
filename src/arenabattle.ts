import Character from "./characters/character";

export default class BattleSimulationTest {    
    arene(listOfChar : Array <Character>){
        // on check que les 2 combattants sonts presents
        // if (!adversaire1 || !adversaire2){
        //     console.log('il manque un ou plusieurs combattants');
        //     // on quitte la methode fight avec return
        //     return;
        // }

    //system attribution combats
    function combatSeed(a : number){
    if (a<listOfChar.length){
        for (let i = a ; i<listOfChar.length ; i++){
            if ((i+1)<listOfChar.length){
            console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
            console.log(listOfChar[a].getName()+" vs "+listOfChar[i+1].getName())
            console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
            let adversaire1 = listOfChar[a]
            let adversaire2 = listOfChar[i+1]

            //system levelup dans le cas ou un character est initialiser avec de l'xp
        adversaire1.setLvl()
        adversaire2.setLvl()

        //store stats
        let restorestat1 = adversaire1.getHealth();
        let restorestat2 = adversaire2.getHealth();
        let resethitstrength1 = adversaire1.getHitStrength();
        let resethitstrength2 = adversaire2.getHitStrength();

        console.log("////////// DEBUT COMBAT //////////")
        let tour = 0
        //combat 1v1
        while (adversaire1.getHealth() > 0 && adversaire2.getHealth() > 0) {
            tour++
            let damage1 = adversaire1.attack(adversaire2.state)

            console.log("---------- Tour n°"+(tour)+" ----------")
            let storeHealth2 = adversaire2.getHealth()
                console.log(adversaire1.getName()+" inflige "+Math.round((damage1)*100)/100+" points de dégats à "+adversaire2.getName())
                adversaire2.setHealth(damage1)
                if ((storeHealth2-damage1) !== adversaire2.getHealth()){
                console.log(adversaire2.getName()+" résiste à "+Math.round((damage1-(storeHealth2-adversaire2.getHealth()))*100)/100+" points de dommages")
                }
                console.log(adversaire2.getName()+" à désormais = "+Math.round((adversaire2.getHealth()*100)/100)+" HP")

            console.log("           ")

            if (adversaire2.getHealth() > 0){
            let damage2 = adversaire2.attack(adversaire1.state)
            tour++
            let storeHealth1 = adversaire1.getHealth()
            console.log("---------- Tour n°"+tour+" ----------")
                console.log(adversaire2.getName()+" inflige "+Math.round(damage2*100)/100+" points de dégats à "+adversaire1.getName())
                adversaire1.setHealth(damage2)
                if ((storeHealth1-damage2) !== adversaire1.getHealth()){
                console.log(adversaire1.getName()+" résiste à "+Math.round((damage2-(storeHealth1-adversaire1.getHealth()))*100)/100+" points de dommages")
                }
                console.log(adversaire1.getName()+" à désormais = "+Math.round(adversaire1.getHealth()*100)/100+" HP")
            }
            console.log("          ")
            if (adversaire1.getHealth()<=0){
                adversaire1.die()
                console.log("\n"+"\n"+"\n")
            }else if (adversaire2.getHealth()<=0){
                adversaire2.die()
                console.log("\n"+"\n"+"\n")
            }
        }

        //restore stats
        adversaire1.health = restorestat1;
        adversaire2.health = restorestat2;
        adversaire1.hitStrength = resethitstrength1;
        adversaire2.hitStrength = resethitstrength2;

        }
        }//fin combat 1v1
        //accède au combat suivant
        a++
        combatSeed(a)
    }//quand il n'y a plus de combats a réaliser
        else{
            return;
        }
    }
    //initialise le systeme d'attribution de combat et le lance
    let a = 0
    combatSeed(a)

    }
}